'use strict';

const electron = require('electron');
const app = electron.app;  // Module to control application life.
const BrowserWindow = electron.BrowserWindow;  // Module to create native browser window.
var {Menu, Tray} = require('electron'); // Allows us to handle the system tray
const {ipcMain} = require('electron'); // Allows communication between processes

// includes Settings and Network
const Settings   = require('electron-settings');
const Network    = require('./lib/network.js');
let network      = new Network(Settings.get('login'), Settings.get('password'));

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
var mainWindow = null;
let tray = null

// Quit when all windows are closed.
app.on('window-all-closed', function() {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform != 'darwin') {
        app.quit();
    }
});

// sets up a way to easily override the minimise to tray functionality
var forceQuit = false;

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', function() {

    tray = new Tray('./assets/tray.png');
    tray.setToolTip('Listening for changes...');
    tray.on('double-click', function() {
        mainWindow.show();
    });

    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 800,
        height: 600,
        title: 'Manage Fit',
        icon: './assets/icon.png',
        webPreferences: {
            nodeIntegrationInWorker: true,
            nodeIntegration: true
		},
    });

    mainWindow.openDevTools();
    // On minimise, hide window and place in system tray
    mainWindow.on('minimize',function(event){
        event.preventDefault();
        mainWindow.hide();
    });

    // on close, hide window and place in system tray
    mainWindow.on('close', function (event) {
        if (!forceQuit) {
            if(!app.isQuitting) {
                event.preventDefault();
                mainWindow.hide();
            }
            return false;
        }
    });

    // Emitted when the window is closed.
    mainWindow.on('closed', function() {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        tray = null;
        mainWindow = null;
    });

    load();

});

// routing logic for initial load
async function load() {
    // if we don't have any network credentials stored, load the login screen
    if (!network.checkCredentialsExist()) {
        loadLogin();
    }
    // if we do, do a login function to check credentials and retrieve latest data
    else {
        await network.postJSON('login', false, async function(data) {
            // if we've received an error, dump them back at login screen
            if (data.type == 'error') {
                Settings.delete('login');
                Settings.delete('password');
                network = new Network();
                load();
            }
            else {
                loadMain(data.data);
            }
        });
    }
}

// Attach listener in the main process with the given ID
ipcMain.on('doLogin', async (event, args) => {
    await network.postJSON('login', args, async function(data) {
        if (data.type == 'error') {
            event.sender.send('loginFailure', data.data);
        }
        else {
            Settings.set('login', args['login'])
            Settings.set('password', args['password']);
            loadMain(data.data);
        }
    });
});

// show main page
function loadMain(data) {

    // load main display file
    mainWindow.loadURL('file://' + __dirname + '/pages/index.html');
    // hide the window
    //mainWindow.hide();

    // set menus
    const contextMenu = Menu.buildFromTemplate([
        {label: 'Show Log', type: 'normal', click: function() { mainWindow.show(); }},
        {label: 'Logout', type: 'normal', click: function() { logout(); }},
        {label: 'Quit', type: 'normal', click: function() { quit(); }},
    ]);
    tray.setContextMenu(contextMenu);

    // build main menu
    const mainMenu = Menu.buildFromTemplate([
        {label: 'Synchronise', type: 'normal', click: async function() { 
            await network.postJSON('login', false, async function(data) {
                // if we've received an error, dump them back at login screen
                if (data.type == 'error') {
                    Settings.delete('login');
                    Settings.delete('password');
                    network = new Network();
                    load();
                }
                else {
                    mainWindow.webContents.send('data', data);
                }
            });
        }},
        {label: 'Logout', type: 'normal', click: function() { logout(); }},
        {label: 'Quit', type: 'normal', click: function() { quit(); }},
    ]);
    
    // set menu items
    //Menu.setApplicationMenu(mainMenu);

    mainWindow.webContents.on('did-finish-load', () => {
        mainWindow.webContents.send('data', data);
    });

}

// upload 
ipcMain.on('doLogin', async (event, args) => {
    await network.postJSON('login', args, async function(data) {
        if (data.type == 'error') {
            event.sender.send('loginFailure', data.data);
        }
        else {
            Settings.set('login', args['login'])
            Settings.set('password', args['password']);
            loadMain(data.data);
        }
    });
});

// load the login screen along with associated menus
function loadLogin() {

    // load login page
    mainWindow.loadURL('file://' + __dirname + '/pages/login.html');

    // build tray menu
    const trayMenu = Menu.buildFromTemplate([
        {label: 'Show', type: 'normal', click: function() { mainWindow.show(); }},
        {label: 'Quit', type: 'normal', click: function() { quit(); }},
    ]);

    // build main menu
    const mainMenu = Menu.buildFromTemplate([
        {label: 'Quit', type: 'normal', click: function() { quit(); }},
    ]);

    // set menu items
    Menu.setApplicationMenu(mainMenu);
    tray.setContextMenu(trayMenu);

}

function quit() {
    forceQuit = true;
    mainWindow.webContents.send('quit');
    app.quit();
}

// handle the logout process
function logout() {

    // I suppose we want to leave all the files intact here?
    // but we do want to stop the directory listener
    mainWindow.webContents.send('stop');

    // and delete all stored credentials
    Settings.delete('login');
    Settings.delete('password');
    Settings.delete('directory');
    network = new Network();

    load();

}