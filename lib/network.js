var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

class Network {

    constructor(login, password) {
        this.login    = login;
        this.password = password;
        this.apiPath  = 'https://www.harboursidefitness.co.uk/api/drive/';
    }

    checkCredentialsExist() {
        if (this.login && this.password) return true;
        return false;
    }

    postJSON(path, data, callback) {

        // gather the post data
        var postData = {};
        if (this.login && this.password) {
            postData['login']    = this.login;
            postData['password'] = this.password;
        }
        if (data) {
            for (var index in data) {
                postData[index] = data[index];
            }
        }

        // create a new ajax request to the specified path
        var request = new XMLHttpRequest();
        request.open('POST', this.apiPath + path, true);
        request.setRequestHeader('Content-Type', "application/json");

        // register the send handler to return the data
        request.onreadystatechange = function() {
            // request is complete
            if (request.readyState == 4) {
                if (request.status == 200) {
                    callback({'type': 'success', 'data': request.responseText});
                }
                else {
                    var type = 'error';
                    var message;
                    if (request.status == 400) {
                        message = 'Malformed Error';
                    }
                    if (request.status == 401 || request.status == 403) {
                        message = 'Not authorised';
                    }
                    if (request.status == 404) {
                        message = 'Page not found';
                    }
                    if (request.status == 500) {
                        message = 'Server Error';
                    }
                    callback({'type': type, 'data': message});
                }
            }
        }

        // send the data
        request.send(JSON.stringify(postData));

    }

}

module.exports = Network;