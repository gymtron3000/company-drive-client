const { ipcRenderer } = require('electron'); // Allows inter process communication

// includes file system, directory listener, and settings
var chokidar = require('chokidar');
var fs       = require('fs');
var file     = require('walk');
var md5      = require('md5');
var ffmetadata = require("ffmetadata");

class FileSystem {

    constructor(directory) {
        this.watcher   = null;
        this.directory = directory;
    }

    // synchronise initial data
    // @param data is incoming information about all stored files/directories from server
    async synchronise(data, callback) {
        this.synchronising = true;
        var self = this;
        this.getExistingFSObjects(function(objects) {
            console.log(objects);
            for (var i = 0; i < objects.length; i++) {
                var object = objects[i];
                console.log(object);
                // check general things like md5 and name match the supplied data
                // if they dont, delete the file
                for (var x = 0; x < data.length; x++) {
                    if (data.name == )
                }
                if (object.path == )
                object.md5.then(function(value) { console.log(object); console.log(value); });
                
            }
        });
    }

    async getExistingFSObjects(callback) {

        var objects = [];

        var walker = file.walk(this.directory);

        walker.on("file", function (root, fileStats, next) {
            objects.push(new FSObject(fileStats, 'file', root));
            next();
        });

        walker.on("directory", function (root, fileStats, next) {
            objects.push(new FSObject(fileStats, 'directory', root));
            next();
        });
         
        walker.on("end", function () {
            walker = null;
            callback(objects);
        });

    }

    startWatcher(callback) {

        this.watcher = chokidar.watch(this.directory, {
            ignored: /[\/\\]\./,
            persistent: true,
            ignoreInitial: true,
        });

        this.watcher.on('add', function(path, fileStats) {
            callback('File ' + path + ' has been added', 'add');
        });
        this.watcher.on('addDir', function(path, fileStats) {
            callback('Directory ' + path + ' has been added', 'addDir');
        });
        this.watcher.on('change', function(path, fileStats) {
            callback("A change ocurred : " + path + " change");
        });
        this.watcher.on('unlink', function(path, fileStats) {
            callback('File ' + path + ' has been removed');
        })
        this.watcher.on('unlinkDir', function(path, fileStats) {
            callback('Directory ' + path + ' has been removed');
        })
        this.watcher.on('error', function(error) {
            callback('Error happened: ' + error);
            console.log(error);
        });

    }

    stopWatcher() {
        this.watcher.close();
        this.watcher = null;
    }

}

module.exports = FileSystem;

class FSObject {
    constructor(fileStats, type, root) {
        this.inode = fileStats.ino;
        this.name  = fileStats.name;
        this.type  = type;
        this.root  = root;
        this.path  = root;
        if (this.type == 'file') {
            this.path += '/' + this.name;
        }
    }
    // gets md5 information from the file. selects readfile or readdir as necessary
    get md5() {
        var self = this;
        return new Promise(function(resolve, reject) {
            if (self.type == 'file') {
                fs.readFile(self.path, function (err, buffer) {
                    resolve(md5(buffer));
                });
            }
            else {
                fs.readdir(self.path, function(error, buffer) {
                    resolve(md5(buffer));
                });
            }
        });
    }
    // gets the updated at metadata
    get updated_at() {
        ffmetadata.read(this.path, function(err, data) {
            return data;
        });
    }
    // set the updated at metadata
    // @param Date object datetime
    set updated_at(datetime) {
        // Y-m-d H:i:s
        var date_format = datetime.getFullYear().toString() + "-" + 
                          pad(datetime.getMonth() + 1) + "-" +
                          pad(datetime.getDate()) + " " +
                          pad(datetime.getHours()) + ":" +
                          pad(datetime.getMinutes()) + ":" +
                          pad(datetime.getSeconds());
        var data = {'updated_at': date_format};
        // write metadata
        ffmetadata.write(this.path(), data);
        // pads numbers to 2 digits
        function pad(n){return n<10 ? '0'+n : n}
    }
}